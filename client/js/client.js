Meteor.subscribe("events");

Template.homePage.helpers({
    events: function () {
        return Events.find({}, {sort: {createdAt: -1}});
    }
});

Template.homePage.events({
    "submit .new-event": function (event) {
        var title = event.target.elements.text.value;
        var date = event.target.elements.date.value;

        if (_.isEmpty(title) || _.isEmpty(date)) {
            alert('Please fill all required fields!');
            return false;
        }

        Meteor.call("addEvent", title, date);

        event.target.elements.text.value = "";
        event.target.elements.date.value = "";

        return false;
    },

    "click .event-entry .delete": function () {
        Meteor.call("deleteEvent", this._id);
    },

    "click .event-entry": function () {
        Router.go('/' + this._id);
    }
});

Template.eventPage.helpers({
    attending: function () {
        var event = Session.get('currentEvent');

        return filterGuestsByStatus(event, 'attending');
    },

    maybe: function () {
        var event = Session.get('currentEvent');

        return filterGuestsByStatus(event, 'maybe');
    },

    notAttending: function () {
        var event = Session.get('currentEvent');

        return filterGuestsByStatus(event, 'not attending');
    }
});

Template.eventPage.events({
    "click .guest-entry .delete": function () {
        var eventId = Session.get('currentEvent')._id;
        Meteor.call("deleteGuest", eventId, this.id);
    },

    "submit .new-guest": function (event) {
        var eventId = Session.get('currentEvent')._id;
        var name = event.target.elements.name.value;
        var status = event.target.elements.status.value;

        if (_.isEmpty(name) || _.isEmpty(status)) {
            alert('Please fill all required fields!');
            return false;
        }

        Meteor.call("addGuest", eventId, name, status);

        event.target.elements.name.value = "";
        event.target.elements.status.value = "attending";

        return false;
    },

    "change .guest-entry .attending-select": function (event) {
        var eventId = Session.get('currentEvent')._id;
        var newStatus = event.target.value;

        Meteor.call("updateGuestStatus", eventId, this.id, newStatus);
    }
});

Template.registerHelper("selectedIfEquals",function(elValue){
    return elValue == this.status ? "selected" : "";
});

Template.registerHelper("isEmpty",function(collection){
    return _.isEmpty(collection);
});

function filterGuestsByStatus(event, status) {
    if (event) {
        return _.filter(event.guests, function (guest) {
            return guest.status === status;
        });
    }
}
