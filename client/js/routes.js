Router.route('/', function () {
    this.render('homePage', {});
});

Router.route('/:eventId', {
    // this template will be rendered until the subscriptions are ready
    loadingTemplate: 'eventPage',

    waitOn: function () {
        // return one handle, a function, or an array
        return Meteor.subscribe('events');
    },

    action: function () {
        var currentEvent = Events.findOne(this.params.eventId);

        Session.set('currentEvent', currentEvent);

        this.render('eventPage');
    }
});

