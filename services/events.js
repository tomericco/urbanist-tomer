Events = new Mongo.Collection("events");

Meteor.methods({
    addEvent: function (title, date) {
        console.log('event added: ' + title + ' ' + date);
        Events.insert({
            title: title,
            date: date,
            createdAt: new Date(),
            guests: [
                {
                    "id": "1",
                    "name": "Mr. Trix",
                    "status": "attending"
                },
                {
                    "id": "2",
                    "name": "Colonel Mustard",
                    "status": "maybe"
                },
                {
                    "id": "3",
                    "name": "George Washington",
                    "status": "attending"

                },
                {
                    "id": "4",
                    "name": "Captain Crunch",
                    "status": "not attending"

                },
                {
                    "id": "5",
                    "name": "Lady Oats",
                    "status": "maybe"
                }
            ]
        });
    },

    deleteEvent: function (id) {
        Events.remove(id);
    },

    deleteGuest: function (eventId, guestId) {
        var event = Events.findOne(eventId);

        if (event) {
            var removedGuest = _.find(event.guests, function (guest) {
                return guest.id === guestId;
            });
            event.guests = _.without(event.guests, removedGuest);

            Events.update(eventId, { $set: { guests: event.guests } });
        } else {
            console.error('Event with id: "' + eventId + '" could not be found for adding a guest');
        }
    },

    addGuest: function (eventId, name, status) {
        var event = Events.findOne(eventId);

        if (event) {
            event.guests.push({
                id: _.random(1000000) + "",
                name: name,
                status: status
            });

            Events.update(eventId, { $set: { guests: event.guests } });
        } else {
            console.error('Event with id: "' + eventId + '" could not be found for adding a guest');
        }
    },

    updateGuestStatus: function (eventId, guestId, status) {
        var event = Events.findOne(eventId);
        var guest;

        if (event) {
            _.forEach(event.guests, function (guest) {
                if (guest.id === guestId) {
                    guest.status = status;
                }
            });

            Events.update(eventId, { $set: { guests: event.guests } });
        } else {
            console.error('Event with id: "' + eventId + '" could not be found for adding a guest');
        }
    }
});
